$(window).scroll(function(){
	var distanciaTop = $(document).scrollTop();
	if(distanciaTop > 200){
		$("header").addClass("diminui-header");
		$("header div").addClass("arredonda");
		$("header div img").addClass("diminui-logo");
	} else {
		$("header").removeClass("diminui-header");
		$("header div").removeClass("arredonda");
		$("header div img").removeClass("diminui-logo");
	}
});