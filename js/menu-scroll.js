function destacaMenu(sessao) {
	var id = sessao.id;

	$("nav li a.scroll-automatico").each(function() {
		var href = this.href.split("#");
		if (href[1] != id) {
			var menu = "#" + href[1];
			$(this).removeClass("selecionado");
		} else {
			$(this).addClass("selecionado");
		}
	});
}

$(document).ready(function() {
	// Muda o menu superior
	$(window).scroll(function() {		
		var largura = $(document).width();
		
		// Se for um desktop
		if(largura > 962){
			if ($("header").offset().top > 50) {

				$("header").css({
					"background-color" : "rgba(255, 255, 255, 1.0)"
				});

				$("header h1").css({
					"text-align" : "left",
					"display" : "inline",
					"float" : "left"
				});

				$("header nav").css({
					"margin-top" : "7px"
				});

				$("header img").css({
					"margin-top" : "12px",
					"margin-bottom" : "-1px",
					"padding-left" : "20px",
					"width" : "110px"
				});

			} else {
				$("header").css({
					"background-color" : "rgba(255, 255, 255, 0.8)"
				});

				$("header h1").css({
					"text-align" : "center",
					"display" : "block",
					"float" : "none"
				});

				$("header img").css({
					"margin-top" : "20px",
					"margin-bottom" : "10px",
					"padding-left" : "0px",
					"width" : "181px"
				});

				$("header nav").css({
					"margin-top" : "0"
				});
			}		
		}
	});
		
		

	// Faz a âncora
	$('a.scroll-automatico').bind('click', function(event) {
		var largura = $(document).width();
		// Se for um desktop
		if(largura < 962){
			$("header nav").slideUp("fast");
		}
		
		var $anchor = $(this);
		var menu_clicado = this;

		$('html, body').stop().animate({
			scrollTop : $($anchor.attr('href')).offset().top
		}, 1300, 'easeInOutExpo', function() {
			// Destaca menu selecionado
			$("nav li a.scroll-automatico").each(function() {
				if (menu_clicado != this) {
					$(this).removeClass("selecionado");
				} else {
					$(menu_clicado).addClass("selecionado");
				}
			});
		});
		event.preventDefault();
	});

	$("#home").mouseover(function() {
		 destacaMenu(this);
	});

	$("#sobre-nos").mouseover(function() {
		 destacaMenu(this);
	});

	$("#servicos").mouseover(function() {
		 destacaMenu(this);
	});
	
	$("#clientes").mouseover(function() {
		 destacaMenu(this);
	});
	
	$("#contato").mouseover(function() {
		 destacaMenu(this);
	});
	
	$("#localizacao").mouseover(function() {
		 destacaMenu(this);
	});
	
});