$(document).ready(
		function() {
			$("#btnEnviar").click(function() {
				$("#erro-contato").fadeOut("fast");
				$("#sucesso-contato").fadeOut("fast");

				$("#txtEmail").removeAttr("style");
				$("#txtNome").removeAttr("style");
				$("#txtMensagem").removeAttr("style");

				var nome = $("#txtNome").val();
				var email = $("#txtEmail").val();
				var mensagem = $("#txtMensagem").val();
				var erro = false;

				if (nome === "") {
					$("#txtNome").css({
						"border" : "solid 1px red"
					});
					$("#erro-contato").fadeIn("fast");
					var erro = true;
				}

				if (email === "") {
					$("#txtEmail").css({
						"border" : "solid 1px red"
					});
					$("#erro-contato").fadeIn("fast");
					var erro = true;
				}

				if (mensagem === "") {
					$("#txtMensagem").css({
						"border" : "solid 1px red"
					});
					$("#erro-contato").fadeIn("fast");
					var erro = true;
				}

				if (erro === false) {
					$("#erro-contato").fadeOut("fast");
					$("#sucesso-contato").fadeIn("fast");
					
				}
				
				$('html body').animate({
					scrollTop : $("#label-foco").offset().top
				}, 500);
				
			});

			$("#hamburger-button").click(function() {
				$("header nav").slideToggle("fast");
			});

			$(".campo-form").focus(function() {
				$(this).animate({
					borderColor : "#ff7e00"
				}, 500, function() {
					$(this).css({
						"box-shadow" : "0 0 5px #ff7e00"
					});
				});
			});

			$(".campo-form").focusout(function() {
				$(this).css({
					"border-color" : "#999999",
					"box-shadow" : "none"
				});
			});

			setTimeout(function() {
				var localizacaoGeografica = new google.maps.LatLng('-23.1634089', '-47.0576902');

				var opcoesDoMapa = {
					zoom : 17,
					scrollwheel : false, // sem scroll do mouse
					center : localizacaoGeografica,
					mapTypeId : google.maps.MapTypeId.ROADMAP
				};

				var divMapa = document.getElementById('mapa');
				var mapa = new google.maps.Map(divMapa, opcoesDoMapa);

				marcador = new google.maps.Marker({
					map : mapa,
					draggable : false,
					animation : google.maps.Animation.DROP,
					position : localizacaoGeografica,
					title : 'Profit Net'
				});

			})

		});